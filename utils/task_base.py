import os
import requests
import shutil
from pyspark.sql import SparkSession, DataFrame


class TaskBase():
    def __init__(self,
                task_name: str,
                task_id: str,
                source_url: str,
                source_format: str
            ) -> None:

        self.task_name = task_name
        self.task_id = task_id
        self.source_url = source_url
        self.source_format = source_format

    def _download_data(self) -> None:
        r = requests.get(url=self.source_url)
        r.raise_for_status()

        with open(f'./{self.task_id}/source_{self.task_name}.{self.source_format}', 'w') as f:
            f.write(r.text)

    def get_spark_context(self) -> SparkSession:
        spark_session_builder = SparkSession.builder.appName(f"MagaLu: {self.task_name}")
        spark_session = spark_session_builder.getOrCreate()
        return spark_session

    def load_dataframe(self) -> DataFrame:
        self._download_data()
        spark = self.get_spark_context()

        if self.task_id == 'task1':
            return spark.read.text(f'./{self.task_id}/source_{self.task_name}.{self.source_format}')
        
        if self.task_id == 'task2':
            return spark.read.csv(f'./{self.task_id}/source_{self.task_name}.{self.source_format}', sep=',', header=True, inferSchema=True)
        
        raise("Task ID not found")

    def save_dataframe(self, df: DataFrame) -> None:

        df.coalesce(1).write.mode("overwrite").option("header", True).csv(f"./{self.task_id}/{self.task_name}")
        
        # remove extra information of write process
        part_filename = next(entry for entry in os.listdir(f'./{self.task_id}/{self.task_name}') if entry.startswith('part-'))
        temporary_csv = os.path.join(f'./{self.task_id}/{self.task_name}', part_filename)
        shutil.copyfile(temporary_csv, f'./{self.task_id}/{self.task_name}.csv')

        self._delete_files(f'./{self.task_id}/source_{self.task_name}.{self.source_format}')
        shutil.rmtree(f'./{self.task_id}/{self.task_name}')

    def _delete_files(self, *args) -> None:
        for file_path in args:
            try:
                os.remove(file_path)
            except:
                self.log.info("Cant remove {}".format(file_path))

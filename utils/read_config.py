from yaml import load, FullLoader


def read_config(config_path: str) -> dict:
    with open(config_path, 'r') as cp:
        config = load(cp, Loader=FullLoader)
        return config['task']

from pyspark.sql.functions import col, regexp_replace, explode, split, lower, when, length, sum
from utils.read_config import read_config
from utils.task_base import TaskBase


# get task config
config = read_config("./task1/config.yaml")
greater_than_number = config['task_config']['numbers_until']

# init class with some features
task1 = TaskBase(config['task_name'], config['task_id'], config['task_source'],
              config['source_format'])
spark = task1.get_spark_context()

df_raw = task1.load_dataframe()

# --------------

df_task1 = df_raw\
    .withColumn('value', regexp_replace(col("value"), "[^a-zA-Z0-9 ]", ""))\
    .withColumn('word', explode(split(lower(col('value')), " ")))\
    .withColumn('is_greater_than', when(length(col('word')) > greater_than_number, True)
                .otherwise(False))\
    .groupBy(['word', 'is_greater_than'])\
    .agg({'word': 'count'})\
    .select(
        'word',
        col('count(word)').alias('count'),
        col('is_greater_than')
    )

words_greater_than = df_task1.filter(col("is_greater_than") == True)\
    .select(sum(col('count'))).collect()[0][0]

# create a dataframe to add the greater than info
greater_than_df = spark.createDataFrame(
    [(f'MAIORES QUE {greater_than_number}', words_greater_than)])

df_final = df_task1\
    .filter(col("is_greater_than") == False)\
    .drop('is_greater_than')\
    .union(greater_than_df)

# --------------

# export to csv
task1.save_dataframe(df_final)

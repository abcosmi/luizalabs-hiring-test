from pyspark.sql.functions import col, to_date, from_unixtime, round, months_between, \
    lit, concat, count, collect_list, split
from utils.read_config import read_config
from utils.task_base import TaskBase


# get task config
config = read_config("./task2/config.yaml")
age_limit = config['task_config']['age_limit']
minimum_orders = config['task_config']['minimum_orders']

# init class with some features
task2 = TaskBase(config['task_name'], config['task_id'],
                 config['task_source'], config['source_format'])


df_raw = task2.load_dataframe()

# --------------

df_less_than_30 = df_raw\
    .withColumn('data_nascimento_cliente', to_date(col('data_nascimento_cliente')))\
    .withColumn('data_pedido', from_unixtime(col('data_pedido')))\
    .withColumn('idade', round(months_between(to_date(lit('2018-12-31')), col('data_nascimento_cliente'))/12))\
    .filter(col('idade') < age_limit)

# convert some columns to array to create a list in the end
df_agg = df_less_than_30\
    .withColumn('pedido_data',
                concat(split(col('codigo_pedido'), ',').cast("array<string>"), 
                        split(to_date(col('data_pedido')), ',').cast("array<date>")))\
    .groupBy('codigo_cliente')\
    .agg(count('codigo_cliente'), collect_list('pedido_data'))\
    .select(
        'codigo_cliente',
        col('count(codigo_cliente)').alias('numero_pedidos'),
        col('collect_list(pedido_data)').cast("string").alias('lista_pedidos')
    ).filter(col('numero_pedidos') > minimum_orders)

df_age_id = df_less_than_30.select('codigo_cliente', 'idade').distinct()

# join to add age on dataframe
cond = [df_age_id.codigo_cliente == df_agg.codigo_cliente]
df_final = df_agg.alias('a')\
    .join(df_age_id.alias('b'), cond, how='left')\
    .select(
        'a.*',
        col('b.idade').cast("integer")
    )

# --------------

# export to csv
task2.save_dataframe(df_final)

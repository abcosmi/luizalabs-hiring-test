## Introdução
Este repositório contém os códigos para o teste de Engenharia de Dados do **Luiza Labs**. O teste consiste em duas atividades distintas que utilizam ***Spark*** para análise de dados. Para resolução dos problemas propostos, foi utilizado o ***Pyspark***.

## Estrutura de diretórios
```
├── Makefile
├── README.md
├── requirements.txt
├── task1
│   ├── config.yaml
│   └── wordcount.py
├── task2
│   ├── config.yaml
│   └── orders.py
└── utils
    ├── __init__.py
    ├── read_config.py
    └── task_base.py
```

## Utilização
### Pré-requisitos
Tenha o **Python 3** instalado na sua máquina. Para mais informações de instalação, clique nos links abaixo.
- Link de instalação para [Linux](https://python.org.br/instalacao-linux/).
- Link de instalação para [MacOs](https://python.org.br/instalacao-mac/).

Copie o link deste repositório e faça uma cópia do mesmo.

`git clone https://gitlab.com/abcosmi/luizalabs-hiring-test`

Vá para o diretório do repositório:

`cd luizalabs-hiring-test`

Para execução sem problemas de versão das bibliotecas utilizadas, por favor utilize um ambiente separado como uma *virtual env*. Para saber mais sobre o virtualenv, [clique neste link.](https://www.treinaweb.com.br/blog/criando-ambientes-virtuais-para-projetos-python-com-o-virtualenv/)

Instale o ambiente virtual:

`pip install virtualenv`

Crie o ambiente:

`virtualenv -p python3 venv`

Ative o ambiente:

`source venv/bin/activate`

Faça a instalação das bibliotecas utilizadas no processo a partir do comando:

`make init`

### Executar as tarefas
Para a execução das duas tarefas execute o comando:

`make run_tasks`

Para a execução de uma tarefa de cada vez, execute os seguintes comandos:

`make run_task1`

`make run_task2`

Os resultados estarão dentro das pastas das respectivas tarefas em formato ***csv***.
testeee
init:
	pip install -r requirements.txt

run_task1:
	export PYTHONPATH=$(PYTHONPATH):$(PWD) && python task1/wordcount.py

run_task2:
	export PYTHONPATH=$(PYTHONPATH):$(PWD) && python task2/orders.py

run_tasks:
	export PYTHONPATH=$(PYTHONPATH):$(PWD) && python task1/wordcount.py && python task2/orders.py
